 //**************HEADER FILES****************
#include<iostream>
#include<conio.h>
#include<stdio.h>

//**************NAMESPACE CREATED************
using namespace std;

//**************CLASS FOR CUSTOMER DETAIL****
class Customer_detail
{
    private:
        char name[50]; //******NAME OF CUSTOMER**************
       long double mobilenumber;//***MOBILE NUMBER OF CUSTOMER******
    public:
        void Input() //*******THIS IS FUNCTION WILL TAKE INPUT OF NAME AND MOBILE NUMBER OF CUSTOMER*******
        {
            cout<<"\nenter customer name:";//********INPUT OF CUSTOMER NAME***********
            cin>>name;

            cout<<"enter mobile number:";//**********INPUT OF MOBILE NUMBER**********
            cin>>mobilenumber;
        }
        void Output() //**********TO SHOW OUTPUT AT END WITH THANK YOU GREETING***************
        {
            cout<<"\n\n\n THANK YOU FOR SHOPPING "<<name;
        }
};

class Product_detail//**********CLASS FOR PRODUCT DETAIL*******
{
    public:
        char product_name[50];//**********PRODUCT NAME******************
        int price,quantity;//********PRICE AND QUANTITY OF PRODUCT******
    public:
        void Indata() //************THIS FUNCTION IS FOR INPUTTING PRODUCT DETAILS WHICH IS PURCHASED BY CUSTOMER**
        {
            cout<<"\n\nenter product name:";//*****PRODUCT NAME INPUT*******
            cin>>product_name;

            cout<<"\nenter price of product:Rs.";//***INPUTING PRICE OF FOLLOWING PRODUCT**
            cin>>price;

            cout<<"\nenter quantity:";//*********INPUTING QUANTITY OF FOLLOWING PRODUCT**
            cin>>quantity;
        }
        void Outdata()//***THIS FUNCTION FOR OUTPUT OF PRODUCT NAME,PRICE AND ITS QUANTITY***
        {
            cout<<price;
            cout<<quantity;
            cout<<"product name"<<product_name;
        }
};

int main()
{   Customer_detail i1;//*****INTIALIZING CUSTOMER DETAIL*******
    int option,j=0, total[1000],k,total_amount=0,paid,change;;
    total[0]=0;

    Product_detail i[1000];//*****INTIALIZING PRODUCT DETAILS WHICH IS PURCHASED BY CUSTOMER****
    char ans;

    cout<<"                              WELCOME TO HAPPY SHOPPING                 ";//***GREETING STATEMENT**
    cout<<"\n                      *************************************            ";
    cout<<"\n TO FILL UP CUSTOMER DETAILS PRESS 1";//*******INSTRUCTION FOR SWITCH CASE******
    cout<<"\n GO TO BILLING PROCESS DIRECTLY PRESS 2";//*******INSTRUCTION FOR SWITCH CASE******
    cout<<"\n enter option here:";
    cin>>option;

    switch(option)//******SWITCH CASE**********
    {
        case 1://*******THIS CASE IS FOR FILLING CUSTOMER DETAIL THEN SWITCHES TO BILLING PROCESS***

            i1.Input();//****TO TAKE INPUT ABOUT CUSTOMER DETAILS BY CALLING INPUT FUNCTION****

        case 2://*****THIS CASE IS FOR DIRECTLY SWITCHING TO BILLING PROCESS WITHOUT FILLING CUSTOMER DETAILS******

            cout<<"                              BILLING PROCESS             ";
            //***********************************USING DO-WHILE LOOP************************************************
            do{
                    i[j].Indata();//********WILL TAKE INPUT OF PRODUCT DETAILS BY CALLING INDATA FUNCTION*****
                    j++;
                    cout<<"Do you want to add more products (y/n)?:";//****CONDITIONAL STATEMENT FOR WHILE LOOP***
                    cin>>ans;
                }while(ans=='y'||ans=='Y');

        //***************************************TO GENRATE BILL OF PURCHASED PRODUCT*****************

                cout<<"\n\n------------------------TRANSACTION DETAILS-------------------------";

            for(k=0;k<j;k++)//********USING FOR LOOP FOR SUMMATION OF TOTAL AMOUNT********************
            {   cout<<endl<<endl;
                total[k]=i[k].price*i[k].quantity;
                total_amount =total_amount+total[k];
            }

            cout<<"P R O D U C T  N A M E       P R I C E(in I.N.R)               Q U A N T I T Y ";
             for(k=0;k<j;k++)
             {
                 cout<<"\n\n"<<i[k].product_name;
                 cout<<"                                "<<i[k].price;
                 cout<<"                               "<<i[k].quantity;
             }

//*****output of final total amount to be paid by customer********
            cout<<" \n\n                                        grand total:Rs."<<total_amount;

            cout<<"\n\nenter the amount paid by customer:Rs.";//****INPUT OF AMOUNT PAID BY CUSTOMER****
            cin>>paid;

            change=paid-total_amount;

            cout<<"\nchange:Rs."<<change;//*******CHANGE REMAINING******

            i1.Output();//********GREETING OUTPUT BY CALLING OUTPUT FUNCTION
    }
    return 0;
}
