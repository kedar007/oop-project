package inheritance;
 import java.util.Scanner;
public class Media // parent class
{
	
	private
		int year,price;
	  String title; 
	  Scanner input=new Scanner(System.in);
    public
    	void title()// function for providing title
        {   
    	
        	System.out.println("ENTER TITLE:");
        	title=input.nextLine();
        }
    void set_year()//function for setting year
    {
    	System.out.println("Enter Year Of Publication:");
    	year=input.nextInt();
    }
    void set_price()//function for setting price
    {
    	System.out.println("Enter Price:");
    	price=input.nextInt();
    }
	int get_price()//function to display price 
	{
		return price;
	}
	int get_year()//function to display publish year
	{
		return year;
	}
	String get_title()//function to display title
	{
		return title;
		
	}
		
	Media()//constructor
	{
		title=null;
		year=0000;
		price=0;
	}
}
class Book extends Media//daughter class book which inherit media class property
{
	private
	int page;
	String author;
	Scanner var=new Scanner(System.in);
	
	public
	Book()//constructor
	{
	page=0;
	author=null;
	}
	void author()
	{
		System.out.println("Enter Author Name:");
		author=var.nextLine();
	}
	void page()
	{
		System.out.println("Number of pages:");
		page=var.nextInt();
	}
	int get_page()
	{
		return page;
	}
	String get_author()
	{
		return author;
		
	}
	
}	
class Cd extends Media//daughter class Cd which inherit media class property
{
	private
	float datasize,playtime;
	Scanner in=new Scanner(System.in);
	public
		Cd()//constructor
		{
		datasize=0;
		playtime=0;
		}
		void size()
		{
			System.out.println("Enter DataSize of cd in mb");
			datasize=in.nextFloat();
		}
		void playtime()
		{
			System.out.println("Enter Playtime lenght of media:");
			playtime=in.nextFloat();
		}
		float get_size()
		{
			return datasize;
		}
		float get_playtime()
		{
			return playtime;
			
		}
}

	class Main
	{
		public static void main(String[] args){
		int i,a,p;
		int password=12345;//PASSWORD to protect admin use
		Scanner enter=new Scanner(System.in);
			Book b[]=new Book[2];
			Cd c[]=new Cd[2];
			for(i=0;i<2;i++)
			{
				b[i]=new Book();
				c[i]=new Cd();
			}
			do{/************************MENU DRIVEN PROGRAM*********************************/
				System.out.println("                       MENU                ");// PASSWORD :12345 for admin use
			System.out.println("To enter books details press 1");   //only for administrator (password protected)
			System.out.println("To enter Cd details press 2");      //only for administrator (password protected)
			System.out.println("To search for books press 3");      // user search function
			System.out.println("To search for Cd press 4");         // user search function
			
			
			a=enter.nextInt();
			
				switch(a)
				
				{
				case 1:
					System.out.println("ENTER PASSWORD:");
					p=enter.nextInt();
					if(p==password){
					for(i=0;i<2;i++)
					{System.out.println("Book"+(i+1));
					
					
						b[i].title();
						b[i].author();
						b[i].set_year();
						b[i].page();
						b[i].set_price();
						
						
					}
					
					}
					else
					{
						System.out.println("INVALID PASSWORD");
					}
					break;
				case 2:
					System.out.println("ENTER PASSWORD:");
					p=enter.nextInt();
					if(p==password){
					for(i=0;i<2;i++)
					{System.out.println("CD"+(i+1));
					
						c[i].title();
						c[i].set_year();
						c[i].playtime();
						c[i].size();
						c[i].set_price();
					}
					}else
					{
						System.out.println("INVALID PASSWORD");
					}
					
					break;
				case 3:
				{
					for(i=0;i<2;i++)
					{
						System.out.println("Book"+(i+1));
						
						
						System.out.println("TITLE:"+b[i].get_title());
						System.out.println("Author Name:"+b[i].get_author());
						System.out.println("Year Of Publication:"+b[i].get_year());
						System.out.println("Number Of page:"+b[i].get_page());
						System.out.println("Price:Rs."+b[i].get_price());
						
					}
					
					
						
				}
				break;
				case 4:
					for(i=0;i<2;i++)
					{System.out.println("CD"+(i+1));
					
					System.out.println("Title:"+c[i].get_title());
					System.out.println("Year Of Publication:"+c[i].get_year());
					System.out.println("DataSize:"+c[i].get_size());
					System.out.println("Playtime:Hour"+c[i].get_playtime());
					System.out.println("Price:Rs."+c[i].get_price());

						
						
						
						

						
					}
				
				
				}
				
			}while(a<5);
		}
	}


