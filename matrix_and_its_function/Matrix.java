package matrix_and_its_function;

import java.util.Scanner;
public class Matrix  //MATRIX CLASS 
{
	private 
	int matrix[][];// MULTIDIMENSIONAL ARRAY FOR STORING VALUE IN  MATRIX
	Scanner in=new Scanner(System.in);
	int r ,c;
	int i,j;
	
	
	public 
 Matrix() //constructor
	{
		r=0;
		c=0;
	
	}
	void create_matrix() //FUNCTION CREATING MATRIX
	{int row , column;
	
	System.out.println("enter how many row u wanna create:");
	row=in.nextInt();;
	System.out.println("enter how many column u wanna create:");
	column=in.nextInt();
		matrix=new int[row][column];
	int a;
		r = row;
		c = column;
		System.out.println("\nfill matrix:");
		for (i=0;i<row;i++)
		{for(j=0;j<column;j++)
		{
			a=in.nextInt();
			matrix[i][j]=a;
			
		}
		}
	}
	void extract() //FUNCTION FOR EXTRACTING
	{
		int b,c,d;
		System.out.println("enter number of row:");
		b=in.nextInt();
		System.out.println("enter number of column:");
		c=in.nextInt();
		
		
		d=matrix[b-1][c-1];
		System.out.println(+d);
	}
	void transpose() // FUNCTION FOR  FINDING TRANSPOSE OF MATRIX 
	{
		int d,row,column;
		row=r;
		column=c;
		int trans[][]=new int[row][column];
		for(i=0;i<row;i++)
		{for(j=0;j<column;j++)
		{
			trans[i][j]=matrix[j][i];
		}
		}
		for(i=0;i<row;i++)
		{for(j=0;j<column;j++)
		{
			d=trans[i][j];
			System.out.println(+d);
		}
		}
	}
	void scalar_multiply()// FUNCTION FOR SCALAR MULTIPLICATION
	{
		int k,d,row,column;
		row=r;
		column=c;
		int sm[][]=new int[row][column];
		System.out.println("Enter the number u wanna multiply:");
		k=in.nextInt();
		for(i=0;i<row;i++)
		{for(j=0;j<column;j++)
		{
			sm[i][j]=k*matrix[i][j];
		}
		}
		for(i=0;i<row;i++)
		{for(j=0;j<column;j++)
		{
			d=sm[i][j];
			System.out.println(+d);
		}
		}
		
	}
	
	
	void addtion( Matrix o1 , Matrix o2 )// ADDING 2 MATRICES 
	{
		int r1,c1,r2,c2;
		
		r1=o1.r;
		r2=o2.r;
		c1=o1.c;
		c2=o2.c;
		int s[][]=new int[r1][c1];
		if(r1==r2 && c1==c2)
		{int row=r1;
		int column=c1;
			for(i=0;i<row;i++)
			{for(j=0;j<column;j++)
			{
				s[i][j]=o1.matrix[i][j]+o2.matrix[i][j];
				System.out.println("sum of two matrices is:"+s[i][j]);
			}
			}
		}
		else
		{
			System.out.println("addition of two matrices is not possible");
		}
		
	}
	Matrix(Matrix o1) //COPY CONSTRUCTOR
	{int a;
		r=o1.r;
		c=o1.c;
		for (i=0;i<r;i++)
		{for(j=0;j<c;j++)
		{
			a=in.nextInt();
			matrix[i][j]=a;
			
		}
		}
	}
	void multiplication(Matrix o1 ,Matrix o2)// MULTIPLICATION FUNCTION
	{
		int r1=o1.r;
		int c1=o1.c;
		int r2=o2.r;
		int c2=o2.c;
		int x[][]=new int[r1][c2];
		int temp[][]=new int[r1][c2];
		int k,d;
		for(i=0;i<r1;i++)
		{
			for(k=0;k<c2;k++)
			{
				x[i][k]=0;
			}
		}
		if(r2==c1)
		{int row=r1;
		int column=c1;
			
		for(i=0;i<row;i++)
		{
			for(k=0;k<c2;k++)
				
		       {for(j=0;j<column;j++)
		            {
			            temp[i][k]=o1.matrix[i][j]*o2.matrix[j][k];
			            x[i][k]=x[i][k]+temp[i][k];
			            
			            
		            }
		        }
	
		
			
		 }
		for(i=0;i<r1;i++)
		{
			for(k=0;k<c2;k++)
			{
				d=x[i][k];
				System.out.println("multiplication is:"+d);
			}
		}
	}
		else
		{
			System.out.println("multiplication of matrices is not possible");
		}
	
}
	
}

	class function{ // MAIN CLASS
	public static void main(String[] args) {
		int op;
		Scanner var=new Scanner(System.in);
		Matrix o1,o2,o3,o4;// CREATING OBJECTS
		o1=new Matrix();
		o2=new Matrix();
		o3=new Matrix();
		o4=new Matrix(o1); // CALLING COPY CONSTRUCTOR
		
		/*MENU 
		 * DRIVEN
		 *  PROGRAMME
		   */
		do{
		System.out.println("------------FUNCTIONAL MENU-----------");
		System.out.println("PRESS '1' TO CREATE TWO MATRICES ");
		System.out.println("PRESS '2' FOR TRANSPOSE OF MATRIX");
		System.out.println("PRESS '3' FOR SCALER MULTIPLICATION OF MATRIX");
		System.out.println("PRESS '4' FOR EXTRACTION OF MATRIX");
		System.out.println("PRESS '5' FOR ADDITION OF TWO MATRICES");
		System.out.println("PRESS '6' FOR MULTIPLICATION OF TWO MATRICES");
		
		op=var.nextInt();
		
		switch (op)//SWITCH CASE CONDITION
		{
		 case 1:
		{System.out.println("create first matrix");
			o1.create_matrix();
			System.out.println("create second matrix");
			o2.create_matrix();
		}
		break;
		 case 2:
		 {int x;
			 System.out.println("for transpose of matrix 1 press 1");
			 System.out.println("for transpose of matrix 2 press 2");
		 x=var.nextInt();
		 if(x==1)
		 {System.out.println("TRANSPOSE  of matrix 1 is as follow");
			 o1.transpose();
		 }
		  else if(x==2)
		 {
			  System.out.println("TRANSPOSE  of matrix 2 is as follow");
		 
             o2.transpose();		 
		 }
		 }
		 break;
		 case 3:
		 {int x;
		 System.out.println("for scalar multiplication of matrix 1 press 1");
		 System.out.println("for scalar multiplication of matrix 2 press 2");
	 x=var.nextInt();
	 if(x==1)
	 {
		 
			 o1.scalar_multiply();}
	 else if(x==2)
	 {
			 o2.scalar_multiply();
	 }
			 break;
		 }
		 case 4:
		 { int x;
		 System.out.println("for extracting any element from  matrix 1 press 1");
		 System.out.println("for extracting any element from  matrix 2 press 2");
	 x=var.nextInt();
	if(x==1){		 
			 o1.extract();
	}
	if(x==2)
	{
			 o2.extract();
	}
			 break;
		 }
		 case 5:
		 {System.out.println("addition of two matrices are as follow");
			 o3.addtion(o1, o2);
		 }
		 break;
		 case 6:
		 {System.out.println("multiplication of two matrices are as follow");
			 o3.multiplication(o1, o2);
		 }
		
		}
		 break;
			
		}while(op<7);
		

	}

}
