#include <iostream>
using std::cout;
using std::cin;
using std::endl;

class Queue {
	public:
		int length;
		int *store;
		int head = 0; //front part of queue
		int tail = 0; //rear part of queue

		Queue(int length) {//function which decide queue length
			this -> length = length;
			store = new int[length];//array which stores number input gets size equal to length
		}

		friend Queue operator+(Queue q1, int number);//function which insert number to queue
		Queue operator--();//function for dequeue
		Queue operator--(int x);//display number which pops out
		friend Queue operator <<(Queue q1, int x);//display queue
};

Queue operator+(Queue q1, int number) {
	q1.store[q1.tail] = number;

	q1.tail++;

	return q1;
}

Queue Queue::operator--() {
	int i=store[head];
	cout << i << endl;
	cout << "\n";

	 if(head<tail)
    {
    head++;
    }
	else
    {
      cout<<"No more number left that can be remove";
	}

	return *this;
}

Queue Queue::operator--(int x) {
	int i=store[head];
	cout << "number removed: " << i << endl;
	cout << "\n";



    if(head<tail)
    {
    head++;
    }
	else
    {
      cout<<"No more number left that can be remove";
	}


	return *this;
}

Queue operator<<(Queue q1, int j) {
	for(j = q1.head; j < q1.tail; j++) {
		cout << q1.store[j] << "\t";
	}



	cout << "\n";

	return q1;
}

int main() {
	int l;
	int c;
	int n;
	cout << "\n";
	cout << "enter length of queue: ";
	cin >> l;
	Queue q(l);
	cout << "\n";
/**********************MENU*****************************/
	for(;;) {
		cout << " For  EnQueue funtion PRESS 1" << endl;
		cout << " For DeQueue function PRESS 2" << endl;
		cout << " view all numbers in the queue PRESS 3" << endl;
		cout << " exit PRESS 4" << endl;
		cout << "your choice: ";
		cin >> c;
		cout << "\n";

		switch(c) {
			case 1:
				if(q.tail == q.length) {
					cout << "cannot add more numbers to the queue" << endl;
					cout << "\n";
				}

				else {
					cout << "enter number: ";
					cin >> n;
					cout << "\n";
					q = q + n;
				}
				break;

			case 2:
				if(q.tail == 0) {
					cout << "there are no more numbers in the queue" << endl;
					cout << "\n";
				}

				else {
					q--;
				}
				break;

			case 3:
				if(q.tail == 0) {
					cout << "there are no numbers in the queue" << endl;
					cout << "\n";
				}

				else {
					q = q << 0;
				}
				break;

			case 4:
				return 0;
		}
	}
}
