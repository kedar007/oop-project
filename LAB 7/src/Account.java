import java.util.Scanner;
 abstract class Account // class Account is created abstract so it cannot have its instances
{
private
float deposit;//data member deposit is used in deposit function 
Scanner in=new Scanner(System.in);//scanner for input during run-time

public
float amount;
float bal;//bal stands for balance this data member is used in function of class Account , class Savings ,class current
int accountnumber=1;//account number data member is initialized with 1 so account creation starts with 1
Account()// Constructor
{
	amount=0;
	accountnumber=1;
	
	
}

 abstract void withdraw();
void deposit()//deposit function is use for depositing the amount deposited by customer to his/her account  
{
	System.out.println("Enter amount deposit by customer:");//
	deposit=in.nextFloat();//value enter for depositing by customer will be hold by deposit data member
	bal=bal+deposit;//increment in balance by deposited value
}
void display()
{
	System.out.println(+bal);//display function for displaying balance of customer's account
	
}
}
 class Savings extends  Account// class Savings is child class of class Account
 {
	 private
	
	 float firstamount;//data member which will hold value of first amount entered by customer during creation of his/her account 
	 int total_month,startyear,startmonth ;//startyear & startmonth is data member hold the value of year & month when account is created
	 float interest;//interest data member is interest amount generated at 3 months after creation of account
	 String name;//name for inputing customer name
	 
	 Scanner input=new Scanner(System.in);//scanner for system input for class Savings
	 
	 public
	 Savings()        //constructor
	 {
		firstamount=0;
		bal=firstamount;
		 total_month=0;
		 startyear=0;
		 startmonth=0;
		 name=null;
		 interest=0;
	 }
	 
	 void saving_create()//account creation function
	 {System.out.println("Enter Customer name:");//input for name of customer
	 name=input.nextLine();
     System.out.println("saving account will open with minimum deposit of Rs.1000");
	 System.out.println("Enter amount by which Customer want to open His/Her account:");
	 firstamount=input.nextFloat();//input for first amount entered by customer
	 

	 if(firstamount>=1000)//condition minimum balance to open any saving account should be 1000 Rs.
	 {
		
		 System.out.println("Enter start year:");
		 startyear=input.nextInt();//input for year when account get create
		 System.out.println("Enter start month:");
		 startmonth=input.nextInt();//input for month when account get create
		 System.out.println("Account number:"+accountnumber);//account number will be given to customer on successful creation of his/her account 
		 accountnumber++;//as on successful creation of account account number will be increment by 1 so new account can get next number
		 bal=firstamount;//balance will be equal to first amount enter by customer
		 
	 }
	 else//if 'if' condition fails 
	 {
		 System.out.println("Minimum Rs.1000 needed to open an account");
		  saving_create();
	 }
	 }
  
	 void withdraw()//withdraw function for money withdrawing
	 {float temp;
		 if(bal>500)//condition for withdraw is that minimum balance should be maintain to Rs.500 
		 {
		    System.out.println("Enter amount Customer want to withdraw");
		    amount=input.nextInt();//input of amount to be withdrawn
		    temp=bal-amount;
		    if(amount<bal)//amount for withdraw should be less than balance 
		    {
		   if(temp>=500)//condition which maintain balance minimum to Rs.500
		      {
		         bal=bal-amount;//if all 'if' condition successfully run then amount of withdraw will be deducted from balance amount
		      }
		   else
		      {
		    	System.out.println("Saving account should have minimum Rs.500 as balance");
		      }
		    }
		    else
		    { 
		    	System.out.println("not enough balance to withdraw ");	
		    }
	     }
		 else
		 {
			 System.out.println("Saving account should have minimum Rs.500 as balance");
		 }
	 }
	 void interest(int cm,int cy)
	 {int irate=10;
	 int k;
	 
if (cy>=startyear )
{
	 total_month=((((cy-startyear)*12)+cm)-startmonth)/3;
	 for(k=0;k<total_month;k++)
	 {
		 interest=((bal*irate)*total_month)/100;
		 bal=bal+interest;
	 }
}
else
{
	System.out.println("INVALID INPUT");
	System.out.println("ENTER current year");
}
	 }
}
 
class Current extends Account
{
	private
	
	float firstamount,overdraft;
	String name;
	
	Scanner var=new Scanner(System.in);
	public
	
	Current()
	{
		firstamount=0;
		name=null;
		overdraft=0;
	}
	void current_create()
	{float temp;
		System.out.println("Enter Customer name:");
		name=var.nextLine();
		System.out.println("Account number:"+accountnumber);
		
		System.out.println("Enter amount by which Customer want to open His/Her account:");
		firstamount=var.nextFloat();
		bal=bal+firstamount;
		System.out.println("Enter overdraft Limit of your Customer(note:limit range Rs.5hundred-Rs.1crore):Rs.");
		temp=var.nextFloat();
		if(temp>=500 && temp<=10000000)
		{
			overdraft=temp;
			accountnumber++;
		}
		else
		{
			System.out.println("(note:limit range Rs.5 hundred-Rs.1 crore)");
			current_create();
		}
			
	}
	void withdraw()
	{float temp;
		System.out.println("Enter the amount you want to withdraw :Rs.");
		amount=var.nextInt();
		temp=bal-amount;
		if(temp>=0)
		{
			bal=temp;
		}
		else
		{
			System.out.println("Not enough balance to withdraw");
		}
	}
	void overdraft()
	{
            bal=bal+overdraft;
            withdraw();
	}
	
}

class Main
{
	public static void main(String[] args)
	{Scanner mainin=new Scanner(System.in);
	int choice1,choice2,choice3,i,j;
		
	Account c1[],s1[];
	Current c2[]=new Current[1000];
	Savings s2[]=new Savings[1000];
	c1=new Current[1000];
	s1=new Savings[1000];	
		
		for(i=0;i<1000;i++)
		{
			 c1[i]=new Current();
			 c2[i]=new Current();
		
		}
		for(j=0;j<1000;j++)
		{
			s1[j]=new Savings();
			s2[j]=new Savings();
		}/************************************MENU DRIVEN PROGRAM*************************/
		
		System.out.println("               WELCOME TO ABC BANK                   ");
		
		int y=0;
		int z=0;
		do{
		System.out.println("                                                      ");
		System.out.println("TO CREATE SAVING ACCOUNT       PRESS  1 ");
		System.out.println("TO CREATE CURRENT ACCOUNT      PRESS  2 ");
		System.out.println("FOR DISPLAY OF SAVING ACCOUNT  PRESS  3 ");
		System.out.println("FOR DISPLAY OF CURRENT ACCOUNT PRESS  4 ");
		System.out.println("Enter Choice:");
		choice1=mainin.nextInt();
		
		
		switch(choice1)
		    {
		     case 1:
		     
				 s2[y].saving_create();

		    	 do{
		    		 System.out.println("FOR DEPOSIT                                PRESS 1");
		    		 System.out.println("FOR WITHDRAW                               PRESS 2");
		    		 System.out.println("EXIT                                       PRESS 3");
                     System.out.println("YOUR CHOICE:");
                     choice2=mainin.nextInt();
                     
                     switch(choice2)
                     {
                     case 1:
                    	 s1[y].deposit();
                    	 break;
                   
                     case 2:
                    	 s2[y].withdraw();
                    	 break;}
                    
                 
		    	 }while(choice2<3);
				 y++; 
				 
		    	 break;
		     case 2:
		    	 c2[z].current_create();
			  do
			  {
				  System.out.println("FOR DEPOSIT    PRESS 1");
				  System.out.println("FOR OVERDRAFT  PRESS 2");
				  System.out.println("FOR WITHDRAW   PRESS 3");
				  System.out.println("EXIT           PRESS 4");
				  System.out.println("Your choice:");
				  choice3=mainin.nextInt();
				  
				  switch(choice3)
				  {
				  case 1:
					  c1[z].deposit();
					  break;
				  case 2:
					  c2[z].overdraft();
					  break;
				  case 3:
					  c2[z].withdraw();
					  break;

					 
				  
				  }
				  
			  }while(choice3<4);
			  z++;
			  break;
		
		     case 3:
		    	 
		       	 int current_month, current_year,savings_account_number;
		       	 System.out.println("Enter account number:");
		       	savings_account_number =mainin.nextInt();
            	 System.out.println("Enter current year:");
            	  current_year=mainin.nextInt();
            	  System.out.println("Enter current month:");
            	  current_month=mainin.nextInt();
            	 s2[savings_account_number-1].interest(current_month, current_year);
            	 s2[savings_account_number-1].display();
            	 break;
		     case 4:
		    	 int current_account_number;
		    	 System.out.println("Enter account number:");
			     current_account_number =mainin.nextInt();
		    	 c2[current_account_number-1].display();
				  break;
            	 }
	    	}
		
		
			
    	while(choice1<3);
 

}
	}